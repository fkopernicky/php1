﻿<?php session_start ();?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Perfect to me</title>
    <link rel="stylesheet" type="text/css" href="styl.css"/>
    <link rel="stylesheet" type="text/css" href="middle.css"/>
    <link rel="stylesheet" type="text/css" href="puppies.css">
    <link rel="stylesheet" type="text/css" href="ourgds.css"/>
    <head profile="http://www.w3.org/2005/10/profile">
	<link rel='icon' href="pics/paw2.ico" type='image/x-icon' />
</head>

<body>
<?php include ("func.php");  include ("createtable.php"); 
if (!isset ($_SESSION["langu"])) 
{
	$_SESSION["langu"]=2;
}
elseif (isset ($_REQUEST["languag"]))
{
	$_SESSION["langu"]=$_REQUEST["languag"];
}
?>

<div id="main">
    <div id="logo">
        <div id="jazyky">
            <a href="<?php if ((string)@$_REQUEST["c"]=='') {$dopln="?";} else {$dopln="";} echo $_SERVER['REQUEST_URI'].$dopln;?>&languag=2" id="slov"></a>
            <a href='<?php if ((string)@$_REQUEST["c"]=='') {$dopln="?";} else {$dopln="";} echo $_SERVER['REQUEST_URI'].$dopln;?>&languag=1' id="engl"></a>
        </div>
    </div> 
<script type="text/javascript">
$('html, body').animate({
    scrollTop: $("#mmenu").offset().top
}, 1000);
</script>

    <div id="mmenu"><div id="menu">
        <a href="index.php"><?php if($_SESSION["langu"]==1){echo "HOME";} elseif($_SESSION["langu"]==2) {echo "DOMOV";}?></a>
        <a href="?c=about-us"><?php if($_SESSION["langu"]==1){echo "ABOUT US";} elseif($_SESSION["langu"]==2) {echo "O NÁS";}?></a>
        <div class="dropdown">
           <a href="?c=our-dogs"><?php if($_SESSION["langu"]==1){echo "OUR DOGS";} elseif($_SESSION["langu"]==2) {echo "NAŠE PSY";}?></a>
           <div class="dropdown-content">
              <?php include ("ourdgs.php");?>
           </div>
        </div>
        <?php if (ispuppies()): ?><a href="?c=puppies"><?php if($_SESSION["langu"]==1){echo "PUPPIES";} elseif($_SESSION["langu"]==2) {echo "ŠTENIATKA";}?></a><?php endif; ?>
        <a href="?c=previous"><?php if($_SESSION["langu"]==1){echo "PREVIOUS LITTERS";} elseif($_SESSION["langu"]==2) {echo "PREDOŠLÉ VRHY";}?></a>
        <a href="?c=contact"><?php if($_SESSION["langu"]==1){echo "CONTACT";} elseif($_SESSION["langu"]==2) {echo "KONTAKT";}?></a>
        <a href="?c=gallery"><?php if($_SESSION["langu"]==1){echo "GALLERY";} elseif($_SESSION["langu"]==2) {echo "GALÉRIA";}?></a>
    </div></div>
    
    <div id="middle"><?php obsah();?></div>
    
    <div id="foot">
        <div id="paws"></div>
		<a href="?c=our-friends" id="ourfriends">OUR FRIENDS</a>
        <a href="" class="instaicon instaiconfo" target="_blank"></a>
        <a href="" class="youicon youiconfo" target="_blank"></a>
        <a href="https://www.facebook.com/michal.ilcik.7" class="fbicon fbiconfo" target="_blank"></a>
        <div id="autor">Copyright © 2017 Kopernický. All rights reserved.</div>
    </div>
</div>
<iframe src="reklama.php" id="reklama"></iframe>
</body>
</html>