﻿<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Dokument bez názvu</title>
<style>
html, body {
    height: 100%;
    padding: 0;
    margin: 0;
	background:url(pics/2.png);
	overflow:hidden;
}
#galleryone
{
	background:url(gallery/<?php echo $_GET["foto"].".jpg?timestamp=".time();?>) no-repeat center;
	background-size:auto 100%;
	width:auto;
	height:100%;
	display:block;
}

#pozition
{
	position:absolute;
	top:10px;
	left:65px;
	font-size:24px;
	font-weight:bold;
	color:#FFF;
}

#next, #prev
{
	cursor:pointer;
	position:absolute;
	left: 0;
	padding-top:25%;
	color:#FFF;
	font-size:50px;
	text-decoration:none;
	background:url(pics/post_c.png);
	width:50px;
	height:100%;
	text-align:center;
}
#next
{
	left:auto;
	right:0px;
}

#next:hover, #prev:hover
{
	background:url(pics/post_c.png);
	background-size:contain;
	color:#F30;
	font-size:60px;
	padding-top:24.5%;
}


#close
{
	position:absolute;
	top:5px;
	left:auto;
	right:65px;
	width:auto;
	height:auto;
	color:#FFF;
	font-size:40px;
	text-decoration:none;
	display:block;
	text-align:center;
	padding:15px 20px 15px 20px;
}

#close:hover
{
	color:#F30;
	border:2px solid #F30;
	font-size:30px;
	top:10px;
}

#bottomfoto
{
	position:absolute;
	bottom:0;
	top:auto;
	width:100%;
	height:150px;
	background:url(pics/menu.png);
	background-size:contain;
}

</style>
</head>

<body>

<?php 

// integer starts at 0 before counting
$i = 0; 
$dir = 'gallery/';
if ($handle = opendir($dir)) {
	while (($file = readdir($handle)) !== false){
		if (!in_array($file, array('.', '..')) && !is_dir($dir.$file)) 
			$i++;
	}
}
?>
<div id="bottomfoto"></div>
<a href="index.php?c=gallery" id="close">&Chi;</a>
<?php if ($_GET["foto"]!=1) echo "<a href='galleryone.php?foto=".($_GET["foto"]-1)."' id='prev' onkeydown='37'>&lsaquo;</a>"; else echo "<a href='galleryone.php?foto=".$i."' id='prev'>&laquo;</a>";?>
<?php if ($_GET["foto"]!=$i) echo "<a href='galleryone.php?foto=".($_GET["foto"]+1)."' id='next' onkeydown='39'>&rsaquo;</a>"; else echo "<a href='galleryone.php?foto=1' id='next'>&raquo;</a>";?>


<div id="galleryone"><div id="pozition"><?php echo $_GET["foto"]." / ".$i;?></div>&nbsp;</div>




</body>
</html>